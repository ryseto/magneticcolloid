//
//  main.cpp
//  MagneticColloid
//
//  Created by Ryohei Seto on 4/25/16.
//  Copyright © 2016 Ryohei Seto. All rights reserved.
//
#include <iostream>
#include <ostream>
#include <string>
#include <stdexcept>
#include <getopt.h>
#include <sstream>
#include "global.h"
#include "Simulation.hpp"
#include "GenerateInitConfig.hpp"
#ifndef GIT_VERSION
#include "VersionInfo.h"
#endif
#define no_argument 0
#define required_argument 1
#define optional_argument 2
using namespace std;

int main(int argc, char **argv)
{
	cout << endl << "MagneticColloid version " << GIT_VERSION << endl << endl;
	string usage = "(1) Simulation\n $ MagneticColloid Pe Configuration_File Parameter_File \
	\n\n OR \n\n(2) Generate initial configuration\n $ MagneticColloid -g Random_Seed [-M]\n";
	double dimensionless_number = 0;
	string numeral, suffix;
	int generate_init = 0;
	string type_init_config = "normal";
	int random_seed = 1;
	bool binary_conf = false;
	bool force_to_run = false;
	bool long_file_name = false;
	string config_filename = "not_given";
	string param_filename = "not_given";
	string stress_rate_filename = "not_given";
	string seq_filename = "not_given";
	string seq_type;
	//string rheology_control = "rate";
	string simu_identifier = "";
	const struct option longopts[] = {
		{"magnetic",          required_argument, 0, 'm'},
		{"generate",          optional_argument, 0, 'g'},
		{"binary",            no_argument,       0, 'n'},
		{"identifier",        required_argument, 0, 'v'},
		{"random-seed",       required_argument, 0, 'a'},
		{"force-to-run",      no_argument,       0, 'f'},
		{"long-file-name",    no_argument,       0, 'l'},
		{"help",              no_argument,       0, 'h'},
		{0, 0, 0, 0},
	};
	
	int index;
	int c;
	while ((c = getopt_long(argc, argv, "hn8flg::a:k:i:v:", longopts, &index)) != -1) {
		switch (c) {
			case 'i':
				stress_rate_filename = optarg;
				break;
			case 'g':
				generate_init = 10;
				if (optarg) {
					if (optarg[0] == 'x') {
						generate_init = 2; ///
					}
				}
				break;
			case 'a':
				random_seed = atoi(optarg);
				break;
			case 'n':
				binary_conf = true;
				break;
			case 'v':
				simu_identifier = optarg;
				break;
			case 'f':
				force_to_run = true;
				break;
			case 'l':
				long_file_name = true;
				break;
			case 'h':
				cerr << usage << endl;
				exit(1);
			case '?':
				/* getopt already printed an error message. */
				break;
			default:
				abort ();
		}
	}
	ostringstream in_args;
	for (int i=0; i<argc; i++) {
		in_args << argv[i] << " ";
	}
	
	if (generate_init >= 1) {
		GenerateInitConfig generate_init_config;
		generate_init_config.generate(random_seed, generate_init);
	} else {
		/*
		 * magnetic moment m
		 * Typical magnetic force: (3 mu m^2)/(4 pi (2a)^4)
		 * Typical Brownian force: kT/a
		 * Dimensionless_number (Pe number) can be defined as the ratio between these two forces:
		 * Typical magnetic force/Typical Brownian force
		 * dimensionless_number = Pe_M = (3 mu m^2) / (64 pi kT a^3)
		 */
		if (optind == argc-3) {
			string Pe_mag = argv[optind++];
			if (getSuffix(Pe_mag, numeral, suffix)) {
				dimensionless_number = atof(numeral.c_str());
			} else {
				errorNoSuffix("magnetic field");
			}
			cout << "Magnetic simulation" << endl;
			config_filename = argv[optind++];
			param_filename = argv[optind++];
			cout << "Magnetic field control: " << dimensionless_number << endl;
			cout << "config_filename: " << config_filename << endl;
			cout << "param_filename: " << param_filename << endl;
		} else {
			cerr << usage << endl;
			exit(1);
		}
		
		vector <string> input_files(5);
		input_files[0] = config_filename;
		input_files[1] = param_filename;
		//input_files[4] = seq_filename;
		Simulation simulation;
		simulation.force_to_run = force_to_run;
		simulation.long_file_name = long_file_name;
		simulation.simulationMagnetic(in_args.str(), input_files, binary_conf,
									  dimensionless_number, suffix, simu_identifier);
	}
	return 0;
}

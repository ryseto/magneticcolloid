struct InputValue{
  std::string type;
  double *value; // a pointer to the actual ParameterSet member
  std::string unit;
};

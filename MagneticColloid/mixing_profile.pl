#!/usr/bin/perl

# Usage:
# $ generateYaplotFile.pl par_[...].dat [force_factor] [y_section]
#
# force_factor: To change the widths of strings to exhibit forces.
# y_section: Visualize the trimed range of the y coordinates.

use Math::Trig;
use IO::Handle;
$y_section = 0;
$yap_radius = 1;
$avenumber = 1000;
$mag = 1; ### magnetic simulation

$particle_data = $ARGV[0];
$output_interval = 1;
if ($#ARGV >= 1){
	$output_interval = $ARGV[1];
}
# Create output file name
$i = index($particle_data, 'par_', 0)+4;
$j = index($particle_data, '.dat', $i-1);
$name = substr($particle_data, $i, $j-$i);
$interaction_data = "int_${name}.dat";

printf "name: ${name}\n";

$outfilename = "phi_${name}.dat";

printf "$outfilename\n";

open (OUT, "> ${outfilename}");
open (IN_particle, "< ${particle_data}");

&readHeader;
$first=1;
$c_traj=0;
$num = 0;

$avecount = 0;
$output = 0;

$deltaL = 1;
$kmax = int ($Lz/$deltaL);

printf "kmax = $kmax\n";

$cnt_av = 0;
for ($k=0; $k<$kmax; $k++) {
	$bin[$k] = 0;
}

my $denominator = $avenumber*($Lx*$deltaL);



while (1){
	if ($cnt_av == $avenumber) {
		for ($k=0; $k<$kmax; $k++) {
			$z = $k * $deltaL - $Lz/2;
			$localphi1 = pi*$bin[$k]/$denominator;
			$localphi = pi*$binall[$k]/$denominator;
			$rphi = $localphi1/$localphi;
			printf OUT "$z $time $localphi1 $localphi $rphi\n ";
		}
		printf OUT "\n ";
		
		$cnt_av = 0;
		for ($k=0; $k<$kmax; $k++) {
			$bin[$k] = 0;
			$binall[$k] = 0;
		}
	}
	if ($time > 1000) {
		$output = 1;
	} else {
		$output = 0;
	}
	
	
	&InParticles;
	
	last unless defined $line;
	if ($output == 1) {
		$cnt_av ++;
		for ($i=0; $i<$np; $i++) {
			$z = $posz[$i]+$Lz/2;
			$k = int ($z / $deltaL);
			if ($magsusceptibility[$i] == 1) {
				$bin[$k] ++;
			}
			$binall[$k] ++;
		}
	}
}


close (OUT);
close (IN_particle);

sub readHeader{
	$line = <IN_particle>;
	$line = <IN_particle>; ($buf, $buf, $np) = split(/\s+/, $line);
	$line = <IN_particle>; ($buf, $buf, $VF) = split(/\s+/, $line);
	$line = <IN_particle>; ($buf, $buf, $Lx) = split(/\s+/, $line);
	$line = <IN_particle>; ($buf, $buf, $Ly) = split(/\s+/, $line);
	$line = <IN_particle>; ($buf, $buf, $Lz) = split(/\s+/, $line);
	for ($i = 0; $i < 9; $i++) {
		$line = <IN_particle>;
	}
	#	for ($i = 0; $i < 24; $i++) {
	#		$line = <IN_interaction>;
	#	}
	#printf "$np, $VF, $Lx, $Ly, $Lz\n";
}

sub InParticles {
	$radius_max = 0;
	$line = <IN_particle>;
	if (defined $line){
		# 1 sys.get_shear_strain()
		# 2 sys.shear_disp
		# 3 getRate()
		# 4 target_stress_input
		# 5 sys.get_time()
		# 6 sys.angle_external_magnetic_field
		
		($buf, $shear_strain, $shear_disp, $shear_rate, $Lz, $time,	$fieldangle) = split(/\s+/, $line);
		$lz2 = $Lz/2;
		# shear_rate/shear_rate0
		# shear_rate0 = Fr(0)/(6 pi eta0 a) = 1/6pi
		printf "$time\n";
		$shear_rate = $shear_rate;
		# h_xzstress << sp << c_xzstressXF << sp << c_xzstressGU << sp << b_xzstress
		# 1: number of the particle
		# 2: radius
		# 3, 4, 5: position
		# 6, 7, 8: velocity
		# 9, 10, 11: angular velocity
		# 12: viscosity contribution of lubrication
		# 13: viscosity contributon of contact GU xz
		# 14: viscosity contributon of brownian xz
		# (15: angle for 2D simulation)
		
		for ($i = 0; $i < $np; $i ++){
			$line = <IN_particle>;
			#			($ip, $a, $x, $y, $z, $vx, $vy, $vz, $ox, $oy, $oz,
			#			$h_xzstress, $c_xzstressGU, $b_xzstress, $angle) = split(/\s+/, $line);
			#
			
			if ($output == 1) {
				# ($ip, $a, $x, $y, $z, $vx, $vy, $vz, $ox, $oy, $oz, $ms, $brownian_pressure, $contact_pressure) = split(/\s+/, $line);
				($ip, $a, $x, $y, $z, $vx, $vy, $vz, $mx, $my, $mz, $ms, $fx, $fy, $fz) = split(/\s+/, $line);
				
				
				#					$magmom_x[$i] = $mx;
				#$magmom_y[$i] = $my;
				#$magmom_z[$i] = $mz;
				$magsusceptibility[$i] = $ms;
				#					$bpressure[$i] = $brownian_pressure;
				#				$con_pressure[$i] = $contact_pressure;
				#					$mm[$i] = sqrt($mx*$mx+$my*$my+$mz*$mz);
				
				#		if (true){
				#			#printf OUTMP "$line";
				#			printf OUTMP "$i $x $y $z $a\n";
				#		}
				$radius[$i] = $a;
				if ($xz_shift) {
					$x += $xz_shift;
					$z += $xz_shift;
				if ($x > $Lx/2) {
					$x -= $Lx;
				}
				if ($z > $Lz/2) {
					$z -= $Lz;
				}
				}
				$posx[$i] = $x;
				$posy[$i] = $y;
				$posz[$i] = $z;
				$momx[$i] = $mx;
				$momy[$i] = $my;
				$momz[$i] = $mz;
				$forcex[$i] =$fx;
				$forcey[$i] =$fy;
				$forcez[$i] =$fz;
				
				if ($radius_max < $a){
					$radius_max = $a;
				}
			}
			
		}
		$c_traj++;
	}
}

sub calcsqdist {
	($x1, $y1, $z1, $x2, $y2, $z2) = @_;
	$dist = ($x1-$x2)*($x1-$x2);
	$dist += ($y1-$y2)*($y1-$y2);
	$dist += ($z1-$z2)*($z1-$z2);
	return $dist;
}



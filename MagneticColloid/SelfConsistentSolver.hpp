//
//  SelfConsistentDipole.hpp
//  MagneticColloid
//
//  Created by Ryohei Seto on 5/24/16.
//  Copyright © 2016 Ryohei Seto. All rights reserved.
//

#ifndef SelfConsistentSolver_hpp
#define SelfConsistentSolver_hpp

#include <vector>
#include <array>
#include "vec3d.h"
#include "cholmod.h"
//#include "SuiteSparseQR.hpp"
//#include <SuiteSparseQR.hpp>
//#include "global.h"

struct ODBlock3 {
	std::array<double, 2> col0;
	std::array<double, 1> col1;
	std::array<double, 1> col2;
	int bla;
};

struct DBlock3 {
	std::array<double, 1> col0;
	std::array<double, 1> col1;
	std::array<double, 1> col2;
};

inline void resetODBlock(struct ODBlock3 &b)
{
	b.col0.fill(0);
	b.col1.fill(0);
	b.col2.fill(0);
}

class SelfConsistentSolver{
	
private:
	int np;
	bool mobile_matrix_done;
	std::vector<struct DBlock3> dblocks;
	std::vector<struct ODBlock3> odblocks;

	int odblocks_nb;
	int dblocks_size;
	cholmod_common chol_c;
	cholmod_dense* chol_moment;
	cholmod_dense* chol_field;
	cholmod_sparse* chol_matrix;
	cholmod_factor* chol_L;
	bool chol_L_to_be_freed;

	std::vector<int> odbrows;
	std::vector<int> odbrows_table;
	std::vector<int> dblocks_cntnonzero;
	
	std::vector<std::vector<int>> odb_layout;
	std::vector<std::vector<int>> db_layout;
	
	void factorizeMatrix();
	/*
	 setColumn(const vec3d &nvec, int jj, double scaledXA, double scaledYB, double scaledYBtilde, double scaledYC) :
	 - appends alpha * |nvec><nvec| and corresponding indices
	 [ 3*jj, 3*jj+1, 3*jj+2 ] to column storage vectors
	 odblocks and odFrows_table
	 - this must be called with order, according to LT filling
	 */
	void setColumn(const vec3d& nvec, int jj,
				   double scaledXA, double scaledYA,
				   double scaledYB, double scaledYBtilde, double scaledYC);
	void allocateMatrix();
	void allocateRessources();
	void completeCholmodMatrix();
	
	void insertODBlock(cholmod_sparse *matrix,
					   const std::vector<int> &index_chol_ix,
					   int top_row_nb,
					   const struct ODBlock3 &offdiagblock);

	void insertDBlock(cholmod_sparse *matrix,
					  const std::vector<int> &index_chol_ix,
					  int top_row_nb,
					  const struct DBlock3 &diagblock);

	void insertODBlockRows(int *matrix_i,
						   const std::vector<int> &index_values,
						   int top_row_nb);

	void insertDBlockRows(int *matrix_i,
						  const std::vector<int> &index_values,
						  int top_row_nb);
	
	void insertBlockColumnIndices(int *matrix_p,
								  const std::vector<int> &pvalues);

	void insertODBlockValues(double *matrix_x,
							 const std::vector<int>& index_chol_ix,
							 const struct ODBlock3& b);

	void insertDBlockValues(double *matrix_x,
							const std::vector<int>& index_chol_ix,
							const struct DBlock3& b);
public:
	~SelfConsistentSolver();
	void init(int np_);
	void resetMatrix(int nb_of_interactions);
	void setDiagBlock(int ii, double value);

	/*
	 setOffDiagBlock(const vec3d &nvec, int ii, int jj, double scaledXA, double scaledYB, double scaledYC) :
	 Sets (ii,jj) block with:
	 -  scaledXA * |nvec><nvec| for FU part
	 -  scaledYB * e_ijk nvec_ij for TU part ( scaledYB is scaledYB_12(lambda) in Jeffrey & Onishi's notations)
	 -  scaledYBtilde * e_ijk nvec_ij    ( scaledYBtilde is scaledYB_12(1/lambda) in Jeffrey & Onishi's notations)
	 -  scaledYC *(1 - |nvec><nvec|) for TW part
	 
	 This must be called with order (ii < jj),
	 because we have to fill according to the lower-triangular
	 storage.
	 */
	void setOffDiagBlock(vec3d& rvec, int jj);
	/*
	 doneBlocks(int i) :
	 - to be called when all terms involving particle i have been added,
	 ie blocks in row i and column i are completed
	 */
	void doneBlocks(int i);
	/*
	 completeResistanceMatrix() :
	 - transforms temporary arrays/vectors used to build resistance
	 matrix into Cholmod objects really used by
	 the solvers
	 - also performs Cholesky factorization
	 - must be called before solving the linear algebra problem
	 - must be called after all terms are added
	 */
	void completeMatrix();
	void setRHS_magfield(vec3d magfield);
	void solve(std::vector<vec3d> &magnetic_moment);
	void solvingIsDone();
	void printMatrix(std::ostream& out, std::string sparse_or_dense);
	
};



#endif /* SelfConsistentDipole_hpp */

//
//  SelfConsistentDipole.cpp
//  MagneticColloid
//
//  Created by Ryohei Seto on 5/24/16.
//  Copyright © 2016 Ryohei Seto. All rights reserved.
//

#include "SelfConsistentSolver.hpp"
using namespace std;

SelfConsistentSolver::~SelfConsistentSolver()
{
	if (!chol_moment) {
		cholmod_free_dense(&chol_moment, &chol_c);
	}
	if (!chol_field) {
		cholmod_free_dense(&chol_field, &chol_c);
	}
	if (!chol_matrix) {
		cholmod_free_sparse(&chol_matrix, &chol_c);
	}
	cholmod_finish(&chol_c);
}

void SelfConsistentSolver::init(int np_)
{
	np = np_;
	// resistance matrix characteristics (see header for matrix description)
	dblocks_size = 3*np; // number of non-zero element in diagonal block
	allocateRessources();
	chol_L_to_be_freed = false;
	
	odb_layout.resize(3);
	odb_layout[0] = vector<int>({0,2});
	odb_layout[1] = vector<int>({1});
	odb_layout[2] = vector<int>({0,2});

	db_layout.resize(3);
	db_layout[0] = vector<int>({0});
	db_layout[1] = vector<int>({1});
	db_layout[2] = vector<int>({2});
	dblocks_cntnonzero.resize(3);
	for (int i=0; i<3; i++) {
		dblocks_cntnonzero[i] = 1;
	}
}

/************* Matrix filling methods **********************/

// Diagonal Blocks Terms, FT/UW version
void SelfConsistentSolver::setDiagBlock(int ii, double value)
{
	dblocks[ii].col0[0] = value;
	dblocks[ii].col1[0] = value;
	dblocks[ii].col2[0] = value;
}

// Off-Diagonal Blocks Terms
void SelfConsistentSolver::setOffDiagBlock(vec3d& rvec, int jj)
{
	double r2 = rvec.sq_norm();
	double r = sqrt(r2);
	double common_factor = -1/(4*M_PI*r*r2);
	odbrows.push_back(3*jj);
	int i = (int)odbrows.size()-1;
	odblocks[i].col0[0] =  common_factor*(3*rvec.x*rvec.x/r2-1);
	odblocks[i].col0[1] =  common_factor*3*rvec.x*rvec.z/r2;
	odblocks[i].col1[0] = -common_factor;
	odblocks[i].col2[0] =  common_factor*(3*rvec.z*rvec.z/r2-1);
	return;
}

void SelfConsistentSolver::insertDBlockValues(double *matrix_x,
											  const vector<int>& index_chol_ix,
											  const struct DBlock3& b)
{
	int pcol0 = index_chol_ix[0];
	int pcol1 = index_chol_ix[1];
	int pcol2 = index_chol_ix[2];
	// diagonal blocks row values (21)
	matrix_x[pcol0] =  b.col0[0];   // column j3
	matrix_x[pcol1] =  b.col1[0];   // column j3+1
	matrix_x[pcol2] =  b.col2[0];   // column j3+2
}

//(only stored elements are shown)
//              | odblocks[t].col0[0]     0                 odblocks[t].col0[1]|
//"A21"			|           0          odblocks[t].col1[0]   0                  |
//              | odblocks[t].col0[1]     0                 odblocks[t].col2[0] |
//
void SelfConsistentSolver::insertODBlockValues(double *matrix_x,
											   const vector<int>& index_chol_ix,
											   const struct ODBlock3& b)
{
	matrix_x[index_chol_ix[0]  ] = b.col0[0]; // A // column j3
	matrix_x[index_chol_ix[0]+1] = b.col0[1];
	matrix_x[index_chol_ix[1]  ] = b.col1[0]; // A // column j3+1
	matrix_x[index_chol_ix[2]  ] = b.col0[1]; // A // column j3+2
	matrix_x[index_chol_ix[2]+1] = b.col2[0];
}

void SelfConsistentSolver::insertBlockColumnIndices(int *matrix_p, const vector<int>& pvalues)
{
	/**
	 Insert the starting indices (pvalues) for the 3 columns corresponding to a column of blocks in a cholmod_sparse::p array.
	 You must to gives a pointer to cholmod_sparse::p[first_column] as matrix_p, *not* the bare cholmod_sparse::p
	 */
	for (int col=0; col<3; col++) {
		matrix_p[col] = pvalues[col];
	}
}

void SelfConsistentSolver::insertDBlockRows(int *matrix_i, const vector<int>& index_values, int top_row_nb)
{
	/**
	 Insert row numbers for the diagonal block with top row top_row_nb in a cholmod_sparse::i array.
	 You need to provide the location of the 6 columns of the block in the cholmod_sparse::i array
	 through a vector of indices index_values.
	 */
	for (int col=0; col<3; col++) {
		int index_start = index_values[col];
		matrix_i[index_start] = top_row_nb+db_layout[col][0];
	}
}

void SelfConsistentSolver::insertODBlockRows(int *matrix_i, const vector<int>& index_values, int top_row_nb)
{
	/**
	 Insert row numbers for an off-diagonal block with top row top_row_nb in a cholmod_sparse::i array.
	 You need to provide the location of the 3 columns of the block in the cholmod_sparse::i array
	 through a vector of indices index_values.
	 */
	for (int col=0; col<3; col++) {
		int index_start = index_values[col];
		vector<int> &layout = odb_layout[col];
		for (int s=0; s<layout.size(); s++) {
			matrix_i[index_start+s] = top_row_nb+layout[s];
		}
	}
}

void SelfConsistentSolver::insertDBlock(cholmod_sparse *matrix, const vector<int>& index_chol_ix,
										int top_row_nb, const struct DBlock3& diagblock)
{
	/**
	 Insert the DBlock diagblock in a cholmod_sparse matrix.
	 You must provide the location of the 6 columns of the block in the cholmod_sparse::i array
	 through a vector of indices index_chol_ix, and the nb of the topmost row (equivalently leftmost column) of the block.
	 */
	insertBlockColumnIndices((int*)matrix->p+top_row_nb, index_chol_ix);
	insertDBlockRows((int*)matrix->i, index_chol_ix, top_row_nb); // left_col_nb is also the top row nb on a diag block
	insertDBlockValues((double*)matrix->x, index_chol_ix, diagblock);
}

void SelfConsistentSolver::insertODBlock(cholmod_sparse *matrix, const vector<int>& index_chol_ix,
										 int top_row_nb, const struct ODBlock3& offdiagblock)
{
	/**
	 Insert the ODBlock offdiagblock in a cholmod_sparse matrix.
	 You must provide the location of the 6 columns of the block in the cholmod_sparse::i array
	 through a vector of indices index_chol_ix, and the nb of of the topmost row of the block.
	 */
	insertODBlockRows((int*)matrix->i, index_chol_ix, top_row_nb);
	insertODBlockValues((double*)matrix->x, index_chol_ix, offdiagblock);
}


/*************** Cholmod Matrix Filling *************
 Cholmod matrices we are using are defined in column major order (index j is column index)
 
 Cholmod matrices are defined as follows:
 - all values are stored in array x ( size nzmax )
 - locations of values are encoded in array p ( size np ):
 values corresponding to column j are x[ p[j] ]  to x[ p[j+1] - 1 ]
 - corresponding rows are stored in array i ( size nzmax ):
 rows corresponding to column j are i[ p[j] ]  to i[ p[j+1] - 1 ]
 
 Hence:
 with p[j]-1 < a < p[j+1]
 . . . . j . . . . . .
 .|         .            |
 .|         .            |
 .|         .            |
 i[a] | . . . .x[a]          |
 .|                      |
 .|                      |
 
 
 *****************************************************/
void SelfConsistentSolver::completeMatrix()
{
	allocateMatrix();
	completeCholmodMatrix();
//	printMatrix(cout, "sparse");
	factorizeMatrix();
}

void SelfConsistentSolver::completeCholmodMatrix()
{
	// this function is commented, but you are strongly advised to read
	// the description of storage in the header file first :)
	
	// the vector index_chol_ix tracks the indices in the i and x arrays of the cholmod matrix for the 3 columns
	vector<int> index_chol_ix(3);
	for (int j=0; j<np; j++) {
		/******* Initialize index_chol_ix for this column of blocks **************/
		// associated with particle j are 3 columns in the matrix:
		// { 3j, ... , 3j+2 }
		// the number of non-zero elements before column 3j is:
		// - 3*j from j diagonal blocks
		// - 5*odbrows_table[j] from odbrows_table[j] off-diagonal blocks
		//
		// the number of non-zero elements before column 3j+1 is:
		// - number of non-zero before column 3j + number of non-zero in column 3*j
		// (in 3j:   dblocks_cntnonzero[0] elements in diagonal block, plus 2*(odbrows_table[j+1]-odbrows_table[j])
		// the number of non-zero elements before column 3j+2 is:
		// - number of non-zero before column 3j+1 + number of non-zero in column 3*j+1
		// (in 3j+1: dblocks_cntnonzero[1] elements in diagonal block, plus 1*(odbrows_table[j+1]-odbrows_table[j])
		
		int j3 = 3*j;
		int num_offdiagblock = odbrows_table[j+1]-odbrows_table[j];
		index_chol_ix[0] = 3*j+5*odbrows_table[j];
		int odnzelm[] = {2, 1, 2}; //number of nonzero elements in offdiagonal block
		for (int col=1; col<3; col++) {
			index_chol_ix[col] = index_chol_ix[col-1]+dblocks_cntnonzero[col-1]+odnzelm[col-1]*num_offdiagblock; // nb before previous + elements in previous
		}
		/********* 1: Insert the diagonal blocks elements *********/
		insertDBlock(chol_matrix, index_chol_ix, j3, dblocks[j]);
		for (int col=0; col<3; col++) {
			index_chol_ix[col] += dblocks_cntnonzero[col];
		}
		/********  2  : off-diagonal blocks blocks elements ***********/
		for (int k = odbrows_table[j]; k<odbrows_table[j+1]; k++) {
			insertODBlock(chol_matrix, index_chol_ix, odbrows[k], odblocks[k]);
			index_chol_ix[0] += 2;
			index_chol_ix[1] += 1;
			index_chol_ix[2] += 2;
		}
	}
	// tell cholmod where the last column stops
	// nb of non-zero elements in column col_nb-1 is 1 (it is the last element of the diagonal)
	int nzero_nb_last_col = 1;
	((int*)chol_matrix->p)[3*np] = ((int*)chol_matrix->p)[3*np-1]+nzero_nb_last_col;
}

void SelfConsistentSolver::solvingIsDone()
{
	cholmod_free_factor(&chol_L, &chol_c);
	cholmod_free_sparse(&chol_matrix, &chol_c);
}

void SelfConsistentSolver::allocateRessources()
{
	dblocks.resize(np); // number of diagonal block
	odbrows_table.resize(np+1); //　why np+1 instead of np?
	cholmod_start(&chol_c);
	int size = 3*np;
	chol_field = cholmod_allocate_dense(size, 1, size, CHOLMOD_REAL, &chol_c);
	chol_L = NULL;
}

void SelfConsistentSolver::allocateMatrix()
{
	// CHOLMOD parameters
	int stype = -1; /* 1 is symmetric, stored upper triangular (UT), -1 is LT */
	int sorted = 0;	/* TRUE if columns sorted, FALSE otherwise */
	int packed = 1;	/* TRUE if matrix packed, FALSE otherwise */
	// allocate
	// nzmax: non-zero values
	auto size  = 3*dblocks.size(); //
	auto nzmax = 3*dblocks.size()+5*odblocks_nb; // off-diagonal
	chol_matrix = cholmod_allocate_sparse(size, size, nzmax, sorted, packed, stype, CHOLMOD_REAL, &chol_c);
}

void SelfConsistentSolver::doneBlocks(int i)
{
	odbrows_table[i+1] = (unsigned int)odbrows.size();
}

void SelfConsistentSolver::factorizeMatrix()
{
	/*reference code */
	chol_c.supernodal = CHOLMOD_SUPERNODAL;
	chol_L = cholmod_analyze(chol_matrix, &chol_c);
	cholmod_factorize(chol_matrix, chol_L, &chol_c);
	if (chol_c.status) {
		// Cholesky decomposition has failed: usually because matrix is incorrectly found to be positive-definite
		// It is very often enough to force another preconditioner to solve the problem.
		cerr << " factorization failed. forcing simplicial algorithm... " << endl;
		chol_c.supernodal = CHOLMOD_SIMPLICIAL;
		chol_L = cholmod_analyze(chol_matrix, &chol_c);
		cholmod_factorize(chol_matrix, chol_L, &chol_c);
		cerr << " factorization status " << chol_c.status << " final_ll ( 0 is LDL, 1 is LL ) " << chol_c.final_ll <<endl;
		chol_c.supernodal = CHOLMOD_SUPERNODAL;
	}
}

void SelfConsistentSolver::resetMatrix(int nb_of_interactions)
{
	odblocks_nb = nb_of_interactions;
	odbrows.clear();
	odblocks.resize(odblocks_nb);
	for (auto &b: odblocks) {
		resetODBlock(b);
	}
	odbrows_table[0] = 0;
}

void SelfConsistentSolver::setRHS_magfield(vec3d magfield)
{
	for (unsigned int i=0; i<np; i++) {
		int i3 = 3*i;
		((double*)chol_field->x)[i3  ] = magfield.x;
		((double*)chol_field->x)[i3+1] = magfield.y;
		((double*)chol_field->x)[i3+2] = magfield.z;
	}
}

void SelfConsistentSolver::solve(vector<vec3d> &magnetic_moment)
{
	chol_moment = cholmod_solve(CHOLMOD_A, chol_L, chol_field, &chol_c);
	auto size = chol_moment->nrow/3;
	for (int i=0; i<size; i++) {
		int i3 = 3*i;
		magnetic_moment[i].x = ((double*)chol_moment->x)[i3  ];
		magnetic_moment[i].y = ((double*)chol_moment->x)[i3+1];
		magnetic_moment[i].z = ((double*)chol_moment->x)[i3+2];
	}
	cholmod_free_dense(&chol_moment, &chol_c);
}

// testing

/*************** Cholmod Matrix Filling *************
 Cholmod matrices we are using are defined in column major order (index j is column index)
 
 Cholmod matrices are defined as follows:
 - all values are stored in array x ( size nzmax )
 - locations of values are encoded in array p ( size np ):
 values corresponding to column j are x[ p[j] ]  to x[ p[j+1] - 1 ]
 - corresponding rows are stored in array i ( size nzmax ):
 rows corresponding to column j are i[ p[j] ]  to i[ p[j+1] - 1 ]
 
 Hence:
 with p[j]-1 < k < p[j+1]
        . . . . j . . . . . .
	 .|         .            |
     .|         .            |
	 .|         .            |
 i[a] | . . . .x[k]          |
     .|                      |
     .|                      |
 
 
 *****************************************************/

void SelfConsistentSolver::printMatrix(ostream& out, string sparse_or_dense)
{
	if (sparse_or_dense == "sparse") {
		//		out << endl<< " chol res " << endl;
		auto size = chol_matrix->nrow;
		for (int j = 0; j<size; j++) {
			for (int k =((int*)chol_matrix->p)[j]; k<((int*)chol_matrix->p)[j+1]; k++) {
				out << j << " " << ((int*)chol_matrix->i)[k] << " " << ((double*)chol_matrix->x)[k] << endl;
			}
		}
	}
	if (sparse_or_dense == "dense") {
		cholmod_dense* dense_res = cholmod_sparse_to_dense(chol_matrix, &chol_c);
		auto size = chol_matrix->nrow;
		for (int i = 0; i<size; i++) {
			// if(i==0){
			// 	for (int j = 0; j < size/6; j++) {
			// 		out << j << "\t \t \t \t \t \t";
			// 	}
			// 	out << endl;
			// }
			for (int j = 0; j<size; j++) {
				out << setw(7) << setprecision(2) << ((double*)dense_res->x)[i+j*size] << "\t";
			}
			out << endl;
		}
		out << endl;
		cholmod_free_dense(&dense_res, &chol_c);
	}
}
